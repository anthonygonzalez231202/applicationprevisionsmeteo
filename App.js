import React, {useState, useEffect} from 'react';
import {Text, View, Image, ScrollView} from 'react-native';
import * as Location from 'expo-location';

export default function App() {
    const [location, setLocation] = useState(null);
    const [forecast, setForecast] = useState(null);
    const [currentWeather, setCurrentWeather] = useState(null);
    const apiKey = '247f4df1f41b3e75dbd3577d57c55c53';

    useEffect(() => {
        (async () => {
            let {status} = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                console.error('donne les perms sur le tel');
                return;
            }

            let location = await Location.getCurrentPositionAsync({});
            setLocation(location);

            const lon = location.coords.longitude;
            const lat = location.coords.latitude;

            fetch(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${apiKey}&units=metric`)
                .then((response) => response.json())
                .then((json) => {
                    setCurrentWeather(json);
                });

            fetch(`http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=${apiKey}&units=metric`)
                .then((response) => response.json())
                .then((json) => {
                    const dailyForecast = [];
                    for (let i = 0; i < json.list.length; i += 8) {
                        dailyForecast.push(json.list.slice(i, i + 8));
                    }
                    setForecast({...json, list: dailyForecast});
                });
        })();
    }, []);

    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 40}}>
            <ScrollView>
                {currentWeather && (
                    <View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{currentWeather.name} {new Date().toLocaleDateString('fr-FR', {
                                day: '2-digit',
                                month: 'long'
                            })}</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text>{currentWeather.weather[0].description}</Text>
                                <Image
                                    style={{ width: 50, height: 50 }}
                                    source={{ uri: `http://openweathermap.org/img/w/${currentWeather.weather[0].icon}.png` }}
                                />
                            </View>
                        </View>
                        <Text style={{fontSize: 56, textAlign: 'center'}}>{currentWeather.main.temp}°</Text>
                        <View style={{ height: 1, width: "100%", backgroundColor: "#000"}} />
                    </View>
                )}
                {forecast ? (
                    <View>
                        <Text style={{fontSize: 20, fontWeight: 'bold'}}>Prévisions</Text>
                        <Text style={{fontSize: 12, color: 'gray'}}>slide up/down right/left</Text>

                        {forecast.list.map((dayForecast, index) => {
                            const formattedDay = new Date(dayForecast[0].dt_txt).toLocaleString('fr-FR', {
                                day: '2-digit',
                                month: '2-digit',
                                year: 'numeric'
                            });
                            return (
                                <View key={index}>
                                    <Text>Date: {formattedDay}</Text>
                                    <ScrollView horizontal={true}>
                                        {dayForecast.map((item, index) => {
                                            const formattedHour = new Date(item.dt_txt).toLocaleString('fr-FR', {
                                                hour: '2-digit',
                                                minute: '2-digit'
                                            });
                                            return (
                                                <View key={index}>
                                                    <View style={{
                                                        margin: 5,
                                                        padding: 10,
                                                        backgroundColor: 'white',
                                                        shadowColor: '#000',
                                                        elevation: 7,
                                                        width: 200,
                                                        height: 150
                                                    }}>
                                                        <Text>Heure: {formattedHour}</Text>
                                                        <Text>Ville: {forecast.city.name}</Text>
                                                        <Text>Temperature: {item.main.temp}°C</Text>
                                                        <Text>Description: {item.weather[0].description}</Text>
                                                        <Image
                                                            style={{width: 50, height: 50}}
                                                            source={{uri: `http://openweathermap.org/img/w/${item.weather[0].icon}.png`}}
                                                        />
                                                    </View>
                                                </View>
                                            );
                                        })}
                                    </ScrollView>
                                </View>
                            );
                        })}
                    </View>
                ) : <Text>Loading...</Text>}
            </ScrollView>
        </View>
    );
}